FROM python:3.9.16-alpine3.17

# Download necessary dependancies
RUN apk add gcc musl-dev libffi-dev

# Define environment variables
ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.0.0

# Install poetry via pip
RUN pip install "poetry==$POETRY_VERSION"

# Define the working directory
WORKDIR /app/ps5-sensbox

# Copy only requirements to cache them in docker layer
COPY poetry.lock pyproject.toml ./

# Deactivate virtualenvs creation (because we are already in a isolated context)
# Install dependancies via poetry
RUN poetry config virtualenvs.create false && \
    poetry install --no-dev --no-interaction  --no-ansi

# Copy project files
COPY ./ ./

# Automatically start the app
CMD [ "python3", ".", "start" ]