# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# --------------------
# - My env variables -
# --------------------

# Home assistant
export HOME_ASSISTANT_CONFIG='~/homeassistant'
export TZ='Europe/Zurich'

# ZWAVE
export ZWAVE_SECRET='N3gRUKSepx0e!pCY'
export ZWAVE_USB_BRIDGE_REF='usb-Silicon_Labs_CP2102N_USB_to_UART_Bridge_Controller_38f076685894eb11864a36703d98b6d1-if00-port0'

# Mosquitto
export MOSQUITTO_CONFIG='~/mosquitto'
export MOSQUITTO_USER='noedu'
export MOSQUITTO_PASS='ps5-mosquitto'

# Netatmo
export NETATMO_USER='noe.dupasquier@hotmail.com'
export NETATMO_PASS='djMTr!pTE9xd$kSg'
export NETATMO_CLIENT_ID='639369cba16653da0109f723'
export NETATMO_CLIENT_SECRET='SdaXl3KQsZhkc85dMLyGm2fCnKMZnoDLM'