$(document).ready(function () {
    // Disabled start and stop but
    $("#startBut").prop("disabled", true);
    $("#stopBut").prop("disabled", true);

    // Constants
    const baseUrl = "https://bbdata.smartlivinglab.ch/";

    const rowTitleMotion = "Motion detection";
    const motionDetectedValue = "8"

    const objectIds = {
        "ms01-temperature": 14067,
        "ms01-humidity": 14069,
        "ms01-illuminance": 14068,
        "ms01-ultraviolet": 14070,
        "ms01-motion": 14071,
        "ms01-battery": 14072,
        "netin01-temperature": 14074,
        "netin01-co2": 14075,
        "netin01-humidity": 14076,
        "netin01-noise": 14077,
        "netin01-pressure": 14078,
        "netout01-temperature": 14079,
        "netout01-humidity": 14080,
        "netout01-battery": 14081
    };

    const units = {
        "none": "",
        "celsius": "°C",
        "percent": "%",
        "luminance": "lx",
        "co2": "ppm",
        "decibel": "dB",
        "pressure": "mbar"
    }

    // Variables
    let bbdataKeys = {};
    let polling = false;
    let pollInterval = 5000; // default value in ms

    // Start button
    $("#startBut").on("click", function () {
        $("#startBut").prop("disabled", true);
        $("#pollInterval").prop("disabled", true);
        $("#loginModalBut").prop("disabled", true);
        $("#logoutBut").prop("disabled", true);
        polling = true;
        pollInterval = $("#pollInterval").val() * 1000 // seconds to milliseconds
        getAllData();
    });

    // Stop button
    $("#stopBut").on("click", function () {
        polling = false;

        // Set default text
        $("#lastUpdate").text("No data");
        setValueEachTableRows("ms01-body", "No data");
        setValueEachTableRows("netin01-body", "No data");
        setValueEachTableRows("netout01-body", "No data");

        $("#startBut").prop("disabled", false);
        $("#pollInterval").prop("disabled", false);
        $("#loginModalBut").prop("disabled", false);
        $("#logoutBut").prop("disabled", false);
    });

    // Login form button
    $("#butLoginBBData").on("click", function () {
        let username = $("#usernameBBData").val();
        let password = $("#passwordBBData").val();
        $("#usernameBBData").val("")
        $("#passwordBBData").val("")

        if (username === "" || password === "") {
            Swal.fire({
                title: 'Error !',
                text: 'You must enter a username and a password',
                icon: 'error',
                confirmButtonText: 'Ok'
            })
            return;
        }

        getAPIKeys(username, password)
    });

    // Logout button
    $("#logoutBut").on("click", function () {
        $("#startBut").prop("disabled", true);
        $("#stopBut").prop("disabled", true);
        bbdataKeys = {}
    });

    function getAllData() {
        if (!polling) return;

        // Get current date
        let lastUpdate = new Date();
        $("#lastUpdate").text(lastUpdate.toLocaleDateString() + " " + lastUpdate.toLocaleTimeString());

        // Get all data
        getData("ms01-temperature", objectIds["ms01-temperature"], units["celsius"]);
        getData("ms01-humidity", objectIds["ms01-humidity"], units["percent"]);
        getData("ms01-illuminance", objectIds["ms01-illuminance"], units["luminance"]);
        getData("ms01-ultraviolet", objectIds["ms01-ultraviolet"], units["none"]);
        getData("ms01-motion", objectIds["ms01-motion"], units["none"]);
        getData("ms01-battery", objectIds["ms01-battery"], units["percent"]);
        getData("netin01-temperature", objectIds["netin01-temperature"], units["celsius"]);
        getData("netin01-co2", objectIds["netin01-co2"], units["co2"]);
        getData("netin01-humidity", objectIds["netin01-humidity"], units["percent"]);
        getData("netin01-noise", objectIds["netin01-noise"], units["decibel"]);
        getData("netin01-pressure", objectIds["netin01-pressure"], units["pressure"]);
        getData("netout01-temperature", objectIds["netout01-temperature"], units["celsius"]);
        getData("netout01-humidity", objectIds["netout01-humidity"], units["percent"]);
        getData("netout01-battery", objectIds["netout01-battery"], units["percent"]);

        // Next poll
        if (polling) {
            setTimeout(getAllData, pollInterval);
        }
    }

    function setValueEachTableRows(rootTableId, value) {
        $("#" + rootTableId).children().each(function () {
            $(this).children().eq(1).text(value);
            $(this).children().eq(2).text(value);
        });
    }


    /* 
    ------------
    - REQUESTS -
    ------------
    */

    // Get keys
    function getAPIKeys(username, password) {
        $.ajax({
            url: baseUrl + "login",
            method: "POST",
            contentType: "application/json",
            dataType: "json",
            data: JSON.stringify({
                "username": username,
                "password": password
            })
        })
            .done(function (data) {
                // Save bbdata keys
                bbdataKeys["userId"] = data["userId"];
                bbdataKeys["secret"] = data["secret"];

                // Enabled start and stop but
                $("#startBut").prop("disabled", false);
                $("#stopBut").prop("disabled", false);

                Swal.fire({
                    title: 'Success !',
                    text: 'You are now connected',
                    icon: 'success',
                    confirmButtonText: 'Ok'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $("#loginModal").modal('hide');
                    }
                })
            })
            .fail(function (data) {
                console.log("Fail: " + JSON.stringify(data));
                Swal.fire({
                    title: 'Error !',
                    text: 'Login failed',
                    icon: 'error',
                    confirmButtonText: 'Ok'
                })
            });
    }

    // Get temperature request
    function getData(elementId, objectId, unit) {
        if (!polling) return

        if (Object.keys(bbdataKeys).length === 0) {
            console.log("No API keys");
            return;
        }

        $.ajax({
            url: baseUrl + "objects/" + objectId + "/values/latest",
            method: "GET",
            contentType: "application/json",
            headers: {
                "bbuser": bbdataKeys["userId"],
                "bbtoken": bbdataKeys["secret"]
            },
        })
            .done(function (data) {
                if (!polling) return;

                // Get object value
                let val = data[0]["value"];

                // Get object time
                let tempIso = data[0]["timestamp"];
                let tempDate = new Date(tempIso);

                // Display values
                let elName = $("#" + elementId).children().eq(0);
                let elValue = $("#" + elementId).children().eq(1);
                let elTime = $("#" + elementId).children().eq(2);

                // Special case for motion detection
                if (elName.text() === rowTitleMotion) {
                    $(elValue).text(val === motionDetectedValue ? "Detected" : "Cleared");
                } else {
                    $(elValue).text(val + " " + unit);
                }

                $(elTime).text(tempDate.toLocaleDateString() + " " + tempDate.toLocaleTimeString());
            })
            .fail(function (data) {
                console.log("Fail: " + JSON.stringify(data));
            });
    }
});