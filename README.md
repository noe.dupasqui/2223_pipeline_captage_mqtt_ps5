# PS5 - Pipeline de captage de données basé sur MQTT

## Informations générales
* **Mandant** : iCoSys
* **Etudiant** : Dupasquier Noé - noe.dupasquier@edu.hefr.ch
* **Professeur** : Rumley Sébastien - sebastien.rumley@hefr.ch
* **Co-superviseur** : Hennebert Jean - jean.hennebert@hefr.ch
* **Assistant** : Montet Frédéric - frederic.montet@hefr.ch
* **Assistant** : Rial Jonathan - jonathan.rial@hefr.ch

## Liens utiles
* **Documents en cours de réalisation**
    * [Cahier des charges](docs/Cahiers des charges/latex/cahier_des_charges.pdf)
    * [Rapport de projet](docs/Rapports/latex/rapport_de_projet.pdf)
* **Dépôts annexes**
    * [ps5-sensbox](https://gitlab.forge.hefr.ch/noe.dupasqui/ps5-sensbox)
        * Logiciel Sensbox contenant les améliorations apportées durant ce PS5
        * Adaptations dans le code pour qu'il soit compatible avec la version Docker du pipeline de ce PS5
    * [mqtt-netatmo-bridge](https://github.com/noedupas/mqtt-netatmo-bridge)
        * Fork d'une librairie pour la récupération des données des capteurs Netatmo Weather
        * Correction d'un bug en lien avec la création d'un topic sur le broker
    * [bbdata-api](https://github.com/noedupas/bbdata-api)
        * Fork du repository (bbdata-api) contenant les versions K8S et Docker de BBData, réalisé par M. Mujynya lors de son PS5 de 2021/2022
        * Adaptations pour le Proof of Concept de l'intégration de MQTT dans BBData

## Contexte
Dans le cadre de mon projet de semestre 5, j'ai été mandaté par l'institut iCoSys pour proposer une amélioration d'un pipeline de captage de données déjà existant qui possède des problèmes non résolus. Actuellement, ce pipeline utilise deux capteurs de données, soit le "Netatmo Weather Station" ainsi que le "Aeotec MultiSensor 6". Ces capteurs sont capables de récupérer de nombreuses données environnementales comme par exemple la température, l'humidité, la pression atmosphérique ainsi que la concentration de CO2 dans l'air.

Ces données sont transmises à un Raspberry Pi à l'aide du protocole MQTT. Le Raspberry Pi contient le logiciel de domotique "Home Assistant" permettant la gestion et la communication avec de nombreux appareils électroniques et capteurs.

Le protocole MQTT utilise le mécanisme "Publish/Subcribe", cela signifie que nous avons un serveur central appelé broker (dans notre cas le Raspberry Pi) et des clients (les capteurs et Home Assistant). Les clients "capteurs" publient leurs données sur des topics du broker et le client "Home Assistant" est abonné à ces topics afin de récupérer les données des capteurs et de les afficher sur son dashboard. Un topic est simplement une arborescence permettant d'organiser les messages dans MQTT.

<img src="/docs/Schemas/Pipeline/schema_pipeline_v1.png" width="50%" />

L'ultime objectif de ce travail est d'intégrer une nouvelle version du pipeline sur une plateforme de stockage de données provenant de capteurs, appelée BBData. Cette plateforme, en plus d'offrir un système de stockage, contient des outils de visulisation et d'analyse de données. BBData est principalement destiné aux chercheurs ayant besoin de ces données pour leurs travaux. La raison de l'existence de ce projet est que l'intégration de cet pipeline sur BBData permettra de centraliser les données provenant de capteurs utilisant MQTT comme protocole de communication et ainsi offrir une nouvelle manière fiable de prendre des mesures.
