\appendix

\section*{Annexes}
\addcontentsline{toc}{section}{Annexes}
\addtocontents{toc}{\protect\setcounter{tocdepth}{0}}

\subsection{Installation de Docker et Docker Compose}
\label{appendix_docker_install}
Voici la suite de commande utilisée pour installer Docker et Docker Compose sur le système d'exploitation Raspberry Pi OS.

\begin{minted}{bash}
sudo apt update
sudo apt install docker.io
sudo usermod aG docker $USER
sudo systemctl enable docker now
\end{minted}

\begin{minted}{bash}
sudo apt-get install libffi-dev libssl-dev
sudo apt install python3-dev
sudo apt-get install -y python3 python3-pip
sudo pip3 install docker-compose
\end{minted}


\newpage


\subsection{Installation d'Home Assistant}
\label{appendix_ha_install}
Ci-dessous se trouve la définition du service Home Assistant dans le fichier de Docker Compose.

\begin{minted}{yaml}
version: '3'
services:
  # Home Assistant
  homeassistant:
    container_name: homeassistant
    image: "ghcr.io/home-assistant/home-assistant:stable"
    volumes:
      - $HOME_ASSISTANT_CONFIG:/config
      - /etc/localtime:/etc/localtime:ro
    restart: unless-stopped
    privileged: true
    network_mode: host
\end{minted}

\vspace{12pt}

\textbf{$\$$HOME$\_$ASSISTANT$\_$CONFIG}: emplacement du répertoire qui contiendra les fichiers de configuration. Dans le cas de ce travail, ce sera le répertoire "/home/pi/homeassistant". Cette variable d'environnement est stockée comme ci-dessous à la fin du fichier ".profile" du répertoire de l'utilisateur utilisé sur le Raspberry Pi.

\begin{minted}{bash}
export HOME_ASSISTANT_CONFIG='~/homeassistant'
\end{minted}

\vspace{12pt}

Pour mettre à jour nos modifications, il existe la commande suivante.

\begin{minted}{bash}
source ~/.profile
\end{minted}

\vspace{12pt}

Afin de tester si la variable d'environnement est bien prise en compte par le système, la commande ci-dessous peut être utilisée.

\begin{minted}{bash}
echo $HOME_ASSISTANT_CONFIG
\end{minted}

\vspace{12pt}

Veuillez noter qu'il est aussi possible d'écrire directement le chemin du répertoire dans le fichier YAML au lieu de le stocker dans une variable d'environnement. Finalement, nous pouvons démarrer et arrêter Home Assistant de la manière suivante.

\begin{minted}{bash}
docker-compose up -d
docker-compose down
\end{minted}


\newpage


\subsection{Installation du service pour les capteurs Aeotec}
\label{appendix_aeotec_install}
Voici la définition du service Z-Wave dans le fichier utilisé par Docker Compose:

\begin{minted}{yaml}
version: '3'
services:
  # Home Assistant
  # ...
  
  # Z-Wave server for multisensor 6
  zwave-js-ui:
    container_name: zwave-js-ui
    image: zwavejs/zwave-js-ui:8
    restart: unless-stopped
    tty: true
    stop_signal: SIGINT
    environment:
      - SESSION_SECRET=$ZWAVE_SECRET
      - ZWAVEJS_EXTERNAL_CONFIG=/usr/src/app/store/.config-db
      - TZ=$TZ
    networks:
      - zwave
    devices:
      - "/dev/serial/by-id/${ZWAVE_USB_BRIDGE_REF}:/dev/zwave"
    volumes:
      - zwave-config:/usr/src/app/store
    ports:
      - '8091:8091' # port for web interface
      - '3000:3000' # port for Z-Wave JS websocket server
      
networks:
  zwave:
  
volumes:
  zwave-config:
  name: zwave-config
\end{minted}


\newpage


Il faut également ajouter les variables d'environnement suivantes dans le fichier ".profile" de l'utilisateur courant.

\begin{minted}{bash}
export ZWAVE_SECRET='N3gRUKSepx0e!pCY'
export ZWAVE_USB_BRIDGE_REF='usb-Silicon_Labs_CP2102N_USB_to_UART
_Bridge_Controller_38f076685894eb11864a36703d98b6d1-if00-port0'
\end{minted}

\vspace{12pt}

Pour la variable \textbf{ZWAVE$\_$SECRET}, il est nécessaire de définir un mot de passe.
\\
Concernant la variable \textbf{ZWAVE$\_$USB$\_$BRIDGE$\_$REF}, sa valeur est trouvable grâce à la commande suivante permettant de lister les supports branchés en USB sur le système. Dans ce cas, il faut récupérer l'identifiant du dongle Z-Stick.

\begin{minted}{bash}
ls /dev/serial/by-id/
\end{minted}

\vspace{12pt}

Il faut maintenant redémarrer les deux services (Home Assistant et Z-Wave JS) et ajouter l'intégration Z-Wave sur Home Assistant. L'interface web du serveur Z-Wave est disponible à l'adresse "http://raspberrypi:8091/". Le serveur Z-Wave offre directement la possibilité de l'intégrer à Home Assistant comme le montre la figure ci-dessous.

\begin{figure}[h]
\centering
\includegraphics[height=3cm]{images/methodes/zwave_activate_ha_.png}
\caption{Activation de l'intégration avec Home Assistant sur le serveur Z-Wave}
\end{figure}

Ensuite, sur Home Assistant, il faut ajouter l'add-on ZWave et entrer l'url WebSocket correspondant. La figure suivante donne un aperçu de l'url utilisé dans le cadre de ce travail.

\begin{figure}[h]
\centering
\includegraphics[height=3cm]{images/methodes/zwave_ha_url_integration.png}
\caption{Ajout de l'intégration sur Home Assistant}
\end{figure}


\newpage


La prochaine étape consiste à connecter les capteurs Aeotec au serveur Z-Wave. Lorsque le système recherche un capteur, il faut mettre le capteur en mode "inclusion", comme montré sur la figure ci-dessous, en appuyant longtemps sur le bouton se trouvant en dessous du capteur.

\begin{figure}[h]
\centering
\includegraphics[height=4cm]{images/methodes/zwave_inclusion_sensor.png}
\caption{Inclusion des capteurs sur Z-Wave JS}
\end{figure}


\newpage


\subsection{Installation du broker MQTT}
\label{appendix_broker_install}
Comme pour les autres services, il faut ajouter sa déclaration de le fichier YAML. Ci-dessous se trouve la configuration utilisée lors du travail.

\begin{minted}{yaml}
version: '3'
services:

  # Home Assistant
  # ...
  
  # Z-Wave server for multisensor 6
  # ...

  # Mosquitto mqtt broker
  mosquitto:
    image: eclipse-mosquitto:2.0.15
    container_name: mosquitto
    restart: unless-stopped
    networks:
      - zwave
    volumes:
      - $MOSQUITTO_CONFIG/config/:/mosquitto/config/:ro
      - $MOSQUITTO_CONFIG/log/:/mosquitto/log/
      - mosquitto-data:/mosquitto/data/
    ports:
      - 1883:1883
      - 9001:9001
      
networks:
  zwave:

volumes:
  mosquitto-data:
    name: mosquitto-data
  zwave-config:
    name: zwave-config
\end{minted}

\vspace{12pt}

Ci-dessous se trouve la variable d'environnement ajoutée dans le fichier ".profile".

\begin{minted}{bash}
export MOSQUITTO_CONFIG='~/mosquitto'
\end{minted}


\newpage


Il faut maintenant créer le fichier "/home/pi/mosquitto/conf/mosquitto.conf" permettant de configurer notre serveur MQTT:

\begin{minted}{bash}
listener 1883
persistence true
persistence_location /mosquitto/data
log_dest file /mosquitto/log/mosquitto.log
allow_anonymous false
password_file /mosquitto/config/password.txt
\end{minted}

\vspace{12pt}

Voici quelques explications à propos des configurations choisies:

\vspace{6pt}

\begin{itemize}
\item \textbf{listener}: le port sur lequel le service va écouter
\item \textbf{persistence}: valeur booléenne représentant si la persistance est activée ou non
\item \textbf{persistence$\_$location}: l'emplacement où les fichiers de persistance seront sauvegardés
\item \textbf{log$\_$dest}: le fichier où les "logs" seront enregistrés
\item \textbf{allow$\_$anonymous}: valeur booléenne représentant si des utilisateurs anonymes peuvent utiliser ou non le broker
\item \textbf{password$\_$file}: le fichier contenant les mots de passe des utilisateurs qui sont autorisés à accéder au broker
\end{itemize}

\vspace{12pt}

Pour créer un utilisateur, il faut lancer la commande suivante depuis le conteneur du service Mosquitto et copier le résultat de la commande dans le fichier\\
"/home/pi/mosquitto/conf/password.txt". Il faudra en premier lieu créer ce fichier s'il n'existe pas déjà dans le container.

\begin{minted}{bash}
docker exec -it mosquitto /bin/sh
mosquitto_passwd -b /mosquitto/config/password.txt <user> <password>
\end{minted}

\vspace{12pt}

Après avoir redémarré le service Mosquitto pour prendre en compte les changements de configuration, il faut retourner sur Home Assistant pour ajouter l'intégration du serveur MQTT, en entrant l'url et le port du broker ainsi que le nom d'utilisateur et le mot de passe créé préalablement. La figure ci-dessous montre les paramètres utilisés lors de la réalisation de ce travail.

\begin{figure}[h]
\centering
\includegraphics[height=6cm]{images/methodes/mosquitto_ha_integration.png}
\caption{Ajout de l'intégration MQTT sur Home Assistant}
\end{figure}


\newpage


\subsection{Installation de Sensbox}
\label{appendix_sensbox_install}
Python est installé par défaut sur le Raspberry Pi, mais il est tout de même nécessaire d'installer Poetry. Lors de ce travail, Python était en version 3.9.2. Ci-dessous se trouvent les commandes utilisées pour installer Poetry et lancer le programme Sensbox avec Poetry.

\begin{minted}{bash}
curl -sSL https://install.python-poetry.org | python3 -
\end{minted}

\vspace{12pt}

Il faut ensuite se déplacer dans le répertoire de Sensbox et exécuter la commande ci-dessous.

\begin{minted}{bash}
poetry install
\end{minted}

\vspace{12pt}

Finalement, le script Sensbox peut être démarré de la manière suivante:

\begin{minted}{bash}
poetry shell
python3 . start
\end{minted}

\vspace{12pt}

Pour sortir de l'environnement virtuel créé par Poetry:

\begin{minted}{bash}
exit
\end{minted}


\newpage


\subsection{Envoi des données des capteurs Aeotec sur le broker MQTT}
\label{appendix_aeotec_2_mqtt}
Afin que Sensbox puisse récupérer les données des capteurs, nous devons les configurer pour qu'ils envoient leurs données dans des topics spécifiques se trouvant sur le broker MQTT. Concernant les capteurs Aeotec, nous devons nous rendre sur l'interface web du serveur Z-Wave. Dans les paramètres, dans la section MQTT, il faut ajouter les informations nécessaires pour établir une connexion avec le serveur Mosquitto. Les paramètres modifiés lors de ce travail sont encadrés sur la figure ci-dessous.

\begin{figure}[h]
\centering
\includegraphics[height=5cm]{images/methodes/zwave2mqtt_config.png}
\caption{Configurations effectuées pour la connexion au serveur MQTT}
\end{figure}

Dans la zone \textbf{Host url}, nous devons indiquer le nom de notre conteneur Mosquitto.
\\
Le champ \textbf{Prefix} représente la racine de la structure de topics. Selon la configuration effectuée ci-dessus, tous les topics commenceront par "zwave/...".
\vspace{12pt}\\
L'onglet \textbf{Gateway} contient les paramètres pour la structure des données des topics ainsi que le choix des informations qui seront envoyées sur ces derniers. Voici un petit aperçu des paramètres utilisés dans le cadre de ce travail.

\begin{figure}[h]
\centering
\includegraphics[height=5cm]{images/methodes/zwave2mqtt_config2.png}
\caption{Configuration des topics MQTT}
\end{figure}


\newpage


Le client MQTT nommé \textbf{MQTT Explorer} \cite{mqtt-explorer} a été utilisé afin de retrouver les noms exacts des topics générés sur le broker MQTT. La récupération de ces noms est utile lors de la configuration de Sensbox car il doit faire le lien entre les topics MQTT et les objets de BBData. La figure ci-dessous montre le contenu d'une donnée d'un topic provenant d'un capteur Aeotec depuis MQTT Explorer.

\begin{figure}[h]
\centering
\includegraphics[height=5cm]{images/methodes/zwave2mqtt_explorer.png}
\caption{Visualisation d'un topic sur MQTT Explorer}
\end{figure}


\newpage


\subsection{Service mqtt-netatmo-bridge}
\label{appendix_service_mqtt_netatmo_bridge}
Ce service a été repris et adapté lors de ce travail et son image se trouve sur le serveur Gitlab de la HEIA-FR, dans le registre des conteneurs du dépôt de ce projet de semestre.

\begin{minted}{yaml}
# MQTT bridge for Netatmo sensors
mqtt-netatmo-bridge:
  image: registry.forge.hefr.ch/noe.dupasqui/
2223_pipeline_captage_mqtt_ps5/ps5-mqtt-netatmo-bridge:latest
  container_name: ps5-mqtt-netatmo-bridge
  networks:
    - zwave
  environment:
    LOGGING_NAME: "mqtt-netatmo-bridge"
    LOG_LEVEL: "info"
    TZ: $TZ
    TOPIC_PREFIX: /netatmo
    NETATMO_USER: $NETATMO_USER
    NETATMO_PASS: $NETATMO_PASS
    NETATMO_CLIENT_ID: $NETATMO_CLIENT_ID
    NETATMO_CLIENT_SECRET: $NETATMO_CLIENT_SECRET
    MQTT_HOST: "mqtt://mosquitto:1883"
    MQTT_USER: $MOSQUITTO_USER
    MQTT_PASS: $MOSQUITTO_PASS
    POLL_INTERVAL_SECONDS: 300
\end{minted}

\vspace{12pt}

Les variables d'environnement utilisées sont définies dans le fichier ".profile" de l'utilisateur courant du Raspberry Pi.

\begin{minted}{bash}
# Mosquitto
export MOSQUITTO_CONFIG='~/mosquitto'
export MOSQUITTO_USER='noedu'
export MOSQUITTO_PASS='ps5-mosquitto'

# Netatmo
export NETATMO_USER='noe.dupasquier@hotmail.com'
export NETATMO_PASS='djMTr!pTE9xd$kSg'
export NETATMO_CLIENT_ID='639369cba16653da0109f723'
export NETATMO_CLIENT_SECRET='SdaXl3KQsZhkc85dMLyGm2fCnKMZnoDLM'
\end{minted}


\newpage


\subsection{Sensbox avec Docker}
\label{appendix_docker_sensbox}
Ci-dessous se trouve le Dockerfile permettant de créer l'image Docker de Sensbox. Ce fichier va préalablement installer les dépendances manquantes, définir les variables d'environnement, installer Poetry et finalement copier les fichiers Sensbox et lancer le programme.

\begin{minted}{text}
FROM python:3.9.16-alpine3.17

# Download necessary dependancies
RUN apk add gcc musl-dev libffi-dev

# Define environment variables
ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VERSION=1.0.0

# Install poetry via pip
RUN pip install "poetry==$POETRY_VERSION"

# Define the working directory
WORKDIR /app/ps5-sensbox

# Copy only requirements to cache them in docker layer
COPY poetry.lock pyproject.toml ./

# Deactivate virtualenvs creation
# (because we are already in a isolated context)
# Install dependancies via poetry
RUN poetry config virtualenvs.create false && \
    poetry install --no-dev --no-interaction  --no-ansi

# Copy project files
COPY ./ ./

# Automatically start the app
CMD [ "python3", ".", "start" ]
\end{minted}


\newpage


L'image Docker généré par le fichier Dockerfile peut maintenant être utilisé dans notre fichier Docker Compose comme ci-dessous.

\begin{minted}{yaml}
# PS5 - Sensbox
ps5-sensbox:
  image: registry.forge.hefr.ch/noe.dupasqui/ps5-sensbox:latest
  container_name: ps5-sensbox
  restart: unless-stopped
  networks:
    - zwave
\end{minted}