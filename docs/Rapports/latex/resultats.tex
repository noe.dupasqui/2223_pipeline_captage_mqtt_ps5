\section{Résultats}
\label{resultats}
Ce chapitre concerne les aspects pratiques du projet. Il permet de décrire et d'expliquer les différentes améliorations effectuées ainsi que leurs résultats.

\subsection{Objectif 2 - Amélioration du système existant}
La première étape de cet objectif est de définir les améliorations à effectuer sur le système. Pour cela, un tableau contenant chaque amélioration avec le temps estimé ainsi que son importance dans le système a été réalisé:

\begin{figure}[h]
\centering
\includegraphics[height=5cm]{images/resultats/obj2/todolist_v1.png}
\caption{Tableau des améliorations du système actuel}
\end{figure}

Par rapport au temps à disposition pour cet objectif, il a été décidé lors d'une séance du projet que les trois améliorations de priorité 1 définies dans le tableau ci-dessus vont être réalisées.


\subsubsection{Amélioration 1 - Scripts de configuration et de démarrage}
\label{obj2_improvement1}
Cette tâche consiste à améliorer les scripts de configuration et de démarrage de Sensbox. Suite à une discussion avec les personnes concernées par le projet, un choix s'est posé, cette amélioration concernera la possibilité d'ajouter un fichier de configuration en option de démarrage du script. Ce fichier sera en JSON et contiendra les liaisons entre les "topics" MQTT et les objets BBData. La structure de ce fichier de configuration suit une structure spéciale qui est vérifiée lors du lancement du script grâce à un schéma JSON.


\newpage


Afin d'avoir un aperçu de la structure du fichier, ci-dessous se trouve un exemple de fichier de configuration.

\begin{figure}[h]
\centering
\includegraphics[height=6cm]{images/resultats/obj2/config_json.png}
\caption{Exemple d'un fichier de configuration JSON pour la liaison topics et objets BBData}
\end{figure}

Il est finalement possible de démarrer le script avec ou sans fichier de configuration en entrée du programme. Si aucun fichier de configuration n'est donné, ce sera alors les configurations internes à Sensbox qui seront prises en compte. Le fichier JSON est donné en entrée du programme grâce  l'option \textbf{--config-sensor} comme le montre la commande suivante.

\begin{minted}{bash}
python . start --config-sensor file.json
\end{minted}

\vspace{12pt}

Veuillez noter qu'il est désormais aussi possible, avec le script \textbf{ps5-senbox} en version \textbf{1.2}, de donner un fichier de configuration JSON afin de spécifier l'instance BBData à utiliser ainsi que les paramètres de connexion pour le broker MQTT. Cet ajout a été appliqué lors de la réalisation du Proof of Concept et est documenté dans le chapitre \textbf{"\nameref{poc_activity4}"}.


\subsubsection{Amélioration 2 - Gestion des timestamps}
Actuellement, le temps récupéré pour indiquer quand une des données du capteur a été récupérée se fait au moment où le script Sensbox reçoit la donnée du capteur via un topic MQTT. Cependant, les capteurs utilisés récupèrent également ces temps au moment de l'acquisition de la mesure et il serait plutôt préférable d'utiliser ces valeurs, qui sont par conséquent un peu plus précises.
\vspace{12pt}\\
Pour les capteurs Aeotec, la mise en place de cette amélioration a été facilitée, car ces capteurs ont été configurés sur le serveur Z-Wave pour qu'ils publient leurs données en format JSON sur les topics et ces dernières possèdent une propriété contenant ce temps d'acquisition. Ci-dessous se trouve un exemple d'une donnée reçue par un capteur de la marque Aeotec.

\begin{minted}{json}
{
  "time": 1669711331023,
  "value": 25.6,
  "nodeName": "multisensor",
  "nodeLocation": "HEIAFR"
}
\end{minted}

\vspace{12pt}

Il a fallu effectuer des modifications dans le script Sensbox pour récupérer cette valeur de temps et la formater correctement pour qu'elle soit acceptée par l'API de BBData.
\vspace{12pt}\\
Concernant les capteurs de la marque Netatmo, il y a un topic spécifique contenant le temps d'acquisition, contrairement aux capteurs Aeotec possédant ce temps dans chaque donnée envoyée. Cela signifie également que chaque topic contient une seule valeur et non pas un objet JSON contenant plusieurs valeurs. Cela cause un problème, car Sensbox traite les données au moment où un topic du broker reçoit un changement provenant du capteur. Afin de clarifier le problème par un exemple concret, cela signifie que quand le topic dédié à la température reçoit une nouvelle valeur, Sensbox va bien recevoir cette nouvelle valeur mais ne recevra pas le temps d'acquisition associé à cette valeur, ce qui est ennuyeux car le script doit entrer un temps d'acquisition dans la requête HTTP destinée à l'envoi de cette donnée sur BBData. Le temps d'acquisition sera tout de même mis à jour dans un topic spécifique et ce temps est unique pour toutes les différentes mesures prises par les capteurs. Il est alors décidé, uniquement pour les capteurs Netatmo, de quand même récupérer ce temps depuis le script Sensbox lors du traitement de l'évènement lié à un changement de valeur au niveau d'un des topics MQTT.


\subsubsection{Amélioration 3 - Réinstaller le système en une ligne de commande}
Cette amélioration a été réalisée grâce à l'utilisation de Docker Compose. Certes, étant donné que tous nos services sont définis dans un fichier YAML, ces derniers peuvent être démarrés en une seule ligne de commande qui est la suivante:

\begin{minted}{bash}
docker-compose up -d
\end{minted}

\vspace{12pt}

Il y a néanmoins encore une application qui n'est actuellement pas démarrée avec Docker Compose et qui est Sensbox. Il faut donc créer une image Docker pour Sensbox à l'aide d'un fichier Dockerfile, et la démarche suivie se trouve à l'annexe \textbf{"\nameref{appendix_docker_sensbox}"}.


\newpage


La principale difficulté lors de la réalisation de ce Dockerfile a été de trouver un moyen de faire fonctionner le gestionnaire de dépendances Poetry dans un conteneur. Effectivement, ce dernier crée par défaut un environnement virtuel Python qui n'est pas utilisable dans le contexte d'un conteneur Docker. Après plusieurs recherches, la solution trouvée consiste à utiliser une commande de Poetry permettant d'empêcher la création d'un environnement virtuel Python lors de l'utilisation de ce dernier. Cela va permettre de résoudre le problème survenant avec Docker et cette solution convient bien à cette situation étant donné que l'utilisation des conteneurs permet déjà d'être dans un contexte virtuel, donc il n'est par conséquent pas nécessaire de recréer un environnement virtuel Python.
\vspace{12pt}\\
Une fois l'image créée et sauvegardée sur le registre des conteneurs sur Gitlab, le service Sensbox peut être ajouté dans le fichier Docker Compose, dont les détails se trouvent aussi dans l'annexe indiqué ci-dessus.


\subsubsection{Amélioration supplémentaire - Envoi des données des capteurs Netatmo sur le broker MQTT}
La première étape à entreprendre pour accéder aux données des capteurs Netatmo en dehors de Home Assistant consiste à se rendre sur "https://dev.netatmo.com", de se connecter et de créer ce que Netatmo appelle une "app". Cette dernière permettra d'obtenir un \textbf{Client ID} ainsi qu'un \textbf{Client Secret} qui seront utilisés par le service Docker qui va être présenté dans ce chapitre.
\vspace{12pt}\\
Ce dernier s'appelle \textbf{mqtt-netatmo-bridge} \cite{mqtt-netatmo-bridge} et est un logiciel libre disponible sur Github. En outre, ce dépôt n'a pas été mis à jour par le créateur depuis un certains temps et a par conséquent des dépendances qui ne sont plus à jour. Après quelques recherches dans les "forks" de ce dépôt, il se trouve qu'un autre utilisateur Github s'était chargé de maintenir l'application. La suite du travail lié à l'intégration de ce service s'est donc basé sur ce dernier \cite{mqtt-netatmo-bridge-2}.
\vspace{12pt}\\
Ce service est ainsi disponible sur Docker Hub \cite{mqtt-netatmo-bridge-2-dockerhub} mais uniquement avec l'architecture AMD64, qui n'est pas compatible avec le Raspberry Pi 4 Model B. Pour résoudre ce problème, il a été décidé de "forker" le dépôt sur le Raspberry, ce qui permettra d'effectuer des modifications dans le code si nécessaire et aussi de créer une image compatible avec l'architecture du Raspberry Pi utilisé lors de ce projet.
\vspace{12pt}\\
Une fois l'image créée et stockée dans le registre des conteneurs du dépôt Gitlab, celle-ci peut être utilisée dans la déclaration du nouveau service sise dans le fichier YAML. Les détails de cette configuration se trouve à l'annexe \textbf{"\nameref{appendix_service_mqtt_netatmo_bridge}"}.
\vspace{12pt}\\
Pourtant, lors du démarrage de ce service, une erreur concernant la lecture d'une propriété indéfinie d'un objet Javascript survenait et bloquait ce dernier. L'explication de la résolution de ce problème se situe dans le chapitre \textbf{"\nameref{obj2_problem}"}.


\subsubsection{Démonstration du pipeline amélioré}
Le livrable de ce deuxième objectif est une démonstration du pipeline en fonction contenant les améliorations effectuées. Pour atteindre cet objectif, une petite application web a été réalisée. Cette application permet de visualiser en temps réel les données des capteurs en les récupérant depuis l'API de BBData à intervalles réguliers. La création d'une image Docker de cette application a permis de la déployer sur un serveur web, dans le cas de ce projet un Raspberry Pi, et qui permet aux personnes présentes à la séance de démonstration d'également tester le pipeline.
\vspace{12pt}\\
Cette application a été réalisée avec JQuery et Bootstrap, car l'utilisation d'un framework est un peu exagéré pour l'envergure de l'application et parce que celle-ci sera uniquement utilisée dans le cadre de la démonstration et ne va donc pas évoluer au fil du temps. Son fonctionnement est simple, il faut d'abord se connecter depuis l'application avec un utilisateur BBData ayant les accès aux capteurs utilisés lors de la démonstration. Il faut ensuite choisir l'intervalle entre les requêtes permettant d'actualiser les données de l'application et appuyer le bouton "Start" pour démarrer l'envoi des requêtes. Nous pouvons au final utiliser le bouton "stop" pour arrêter l'envoi des requêtes et le bouton "Logout" pour déconnecter l'utilisateur BBData. Ci-dessous, un aperçu de l'interface de l'application de démonstration.

\begin{figure}[h]
\centering
\includegraphics[height=8cm]{images/resultats/obj2/webapp_demo_1.png}
\caption{Aperçu de l'application web de démonstration (1)}
\end{figure}


\newpage


Au bas de la page de l'application se trouvent les boutons pour la connexion et la déconnexion de l'utilisateur BBData ainsi que les boutons pour démarrer et arrêter l'envoi des requêtes sur l'API, comme le montre la figure ci-dessous.

\begin{figure}[h]
\centering
\includegraphics[height=1cm]{images/resultats/obj2/webapp_demo_2.png}
\caption{Aperçu de l'application web de démonstration (2)}
\end{figure}

La démonstration a permis de prouver qu'il était désormais possible, grâce aux améliorations effectuées lors de ce travail, de démarrer et d'arrêter l'entièreté du pipeline en une seule ligne de commande, de récupérer les données des deux types de capteurs et pour finir de spécifier un fichier de configuration JSON pour la liaison des topics MQTT avec leurs équivalents en objets BBData lors du démarrage du script Sensbox.


\subsubsection{Problèmes rencontrés}
\label{obj2_problem}
\paragraph{Problème du service Docker pour les capteurs Netatmo (mqtt-netatmo-bridge)}
~~\vspace{6pt}\\
Même après plusieurs tentatives de démarrage du service avec des configurations différentes, l'erreur suivante survenait en permanence:

\begin{figure}[h]
\centering
\includegraphics[height=1.8cm]{images/resultats/obj2/netatmo2mqtt_error_undefined.png}
\caption{Erreur dans les logs du service "mqtt-netatmo-bridge"}
\end{figure}

Il a alors fallu regarder plus en détails dans le code de la librairie pour comprendre l'erreur et trouver sa résolution. Après plusieurs tests pour localiser l'emplacement exact de l'erreur, il s'est avéré que le problème survient au moment où le programme envoie les données provenant du capteur extérieur de Netatmo. En effet, une certaine fonction du programme s'occupe de récupérer le nom du module courant, soit le nom du module intérieur ou extérieur, et ce dernier est ensuite utilisé dans le nom des topics. Le problème est que ce nom existe uniquement lorsque le programme traite les données provenant du module intérieur. Lorsqu'il s'agit du module extérieur, le nom est indéfini, ce qui provoque l'erreur ci-dessus. Cette erreur pourrait provenir d'un changement dans la structure des données de l'API de Netatmo, qui n'a pas été pris en compte par la librairie. La figure ci-dessous montre l'emplacement exact dans le code de la librairie où l'erreur apparaît.

\begin{figure}[h]
\centering
\includegraphics[height=5.8cm]{images/resultats/obj2/netatmo2mqtt_error_undefined_2.png}
\caption{Source de l'erreur dans le code du service}
\end{figure}


\newpage


L'ajout d'une condition permettant de séparer la manière d'utiliser les topics pour les modules intérieur et extérieur a permis de résoudre ce problème. Cette modification peut être visualisée sur l'image ci-dessous.

\begin{figure}[h]
\centering
\includegraphics[height=3.9cm]{images/resultats/obj2/netatmo2mqtt_error_undefined_3.png}
\caption{Résolution de l'erreur du nom du module indéfini. Ligne \textbf{214}, fonction \textbf{processModule} et fichier \textbf{mqtt-netatmo-bridge.js}}
\end{figure}

Avec ce changement, le problème a été résolu et le broker MQTT reçoit désormais les données des deux modules Netatmo. Quelques modifications ont également été apportées au script Sensbox afin que ce dernier puisse gérer correctement les données provenant des topics générés par les capteurs Netatmo et Aeotec, car les données de Aeotec sont au format JSON et contiennent plusieurs valeurs numériques, tandis qu'une donnée d'un capteur Netatmo correspond uniquement à une valeur numérique.


\newpage


%-----------------------------------------------------------------------------------------------------
\subsection{Objectif 3 - PoC de l'intégration du système sur BBData}
Cet objectif a pour but de trouver la meilleure approche pour intégrer le système dans BBData et de démontrer ainsi sa faisabilité.

\subsubsection{Activités détaillées du PoC}
Ci-dessous se trouve la liste des tâches qui vont être effectuées lors de ce Proof of Concept:

\vspace{6pt}

\begin{itemize}
\item Activité 1 - Rechercher et documenter des différentes approches possibles pour l'intégration sur BBData
\item Activité 2 - Comparer les approches selon certains critères pour définir la plus pertinente dans notre cas d'utilisation
\item Activité 3 - Mettre en place une infrastructure de test de BBData
\item Activité 4 - Réaliser une première version (prototype) de l'intégration
\item Activité 5 - Documenter et évaluer les résultats du prototype de l'intégration
\end{itemize}


\subsubsection{Activité 1 - Recherche des approches possibles}
Ce chapitre contient toutes les différentes approches trouvées pouvant être applicables pour cette intégration.

\paragraph{Solution 1 - Broker MQTT sur le Raspberry Pi et clients MQTT sur BBData:}
~~\vspace{6pt}\\
Une solution possible est d'intégrer une nouvelle fonctionnalité dans BBData permettant d'ajouter des clients MQTT. Cette fonctionnalité permet à BBData de s'inscrire sur plusieurs topics de plusieurs brokers MQTT, et ainsi le script Sensbox pourrait être retiré du pipeline car la communication s'effectue directement entre les différents broker et BBData. Cette approche garde tout de même le broker du côté du Raspberry Pi, ce qui fait qu'il y aura autant de broker MQTT qu'il y a d'emplacements de prise de mesure. Cette approche est représentée dans le diagramme de composants ci-dessous.

\begin{figure}[h]
\centering
\includegraphics[height=4cm]{images/resultats/obj3/obj3-sol1_v2.png}
\caption{Diagramme de composants de la solution 1}
\end{figure}


\newpage


\paragraph{Solution 2 - Envoie de requêtes HTTP sur l'API de BBData depuis des scripts sur HA:}
~~\vspace{6pt}\\
Une autre possibilité serait d'utiliser l'API de BBData, mais en effectuant les requêtes directement depuis Home Assistant au lieu de passer par un logiciel dédié à cette tâche. Cette solution est possible grâce aux fonctionnalités d'automatisation et de scripting intégrées dans Home Assistant. Le broker MQTT et Sensbox pourraient ainsi être enlevés du pipeline. Ci-dessous se trouve un schéma de la situation.

\begin{figure}[h]
\centering
\includegraphics[height=5cm]{images/resultats/obj3/obj3-sol2.png}
\caption{Diagramme de composants de la solution 2}
\end{figure}


\paragraph{Solution 3 - Broker MQTT dans sous-réseau de BBData et clients MQTT sur BBData:}
~~\vspace{6pt}\\
La troisième option consiste à avoir un unique broker MQTT qui se trouve dans le même sous-réseau que la plateforme BBData. De ce fait, les différents Raspberry Pi envoient les données récoltées sur un seul et unique broker. En fin de compte, BBData contient un client MQTT s'inscrivant aux différents topics, comme vous pouvez le voir sur le diagramme suivant.

\begin{figure}[h]
\centering
\includegraphics[height=4cm]{images/resultats/obj3/obj3-sol3_v2.png}
\caption{Diagramme de composants de la solution 3}
\end{figure}


\newpage


\paragraph{Solution 4 - Alternative de la troisième solution avec un collecteur:}
~~\vspace{6pt}\\
Cette quatrième et dernière solution consiste placer le broker du côté du sous-réseau de BBData, tout comme la solution précédente. La différence se trouve entre le broker et BBData. Cette fois-ci, au lieu d'intégrer un client MQTT dans BBData, un collecteur fait la passerelle entre le broker MQTT et BBData. Cette alternative permet de ne pas toucher au code de BBData, car ce collecteur va contenir le client MQTT et va utiliser l'API de BBData pour envoyer les données sur la plateforme. La situation est représentée par le diagramme ci-dessous.

\begin{figure}[h]
\centering
\includegraphics[height=6cm]{images/resultats/obj3/obj3-sol4_v2.png}
\caption{Diagramme de composants de la solution 4}
\end{figure}


\subsubsection{Activité 2 - Comparaison des approches}
Toutes ces approches pourraient tout à fait être réalisables, mais certaines sont toutefois plus adaptées dans le cas de ce travail. Ce chapitre compare les différentes solutions afin de cibler celle qui sera implémentée dans ce Proof of Concept.
\vspace{12pt}\\
La première solution est intéressante, car la communication se fait directement entre le broker et BBData sans passer par une application intermédiaire. Le protocole MQTT a l'avantage d'être fiable et consomme peu d'énergie. Ce dernier est également mentionné dans le titre de ce projet et fait par conséquent partie intégrante de ce travail. Cependant, à chaque fois qu'on ajoute une nouvelle récolte de données à un nouvel emplacement, nous ajoutons également un broker MQTT. Il serait par conséquent plus logique de créer un seul broker MQTT dans le même sous-réseau que BBData recevant les données de tous les Raspberry Pi. De plus, avec cette solution, BBData doit pouvoir communiquer avec chacun des brokers se trouvant à l'extérieur de son sous-réseau, ce qui peut causer des problèmes de NAT et il faudra donc probablement utiliser des techniques de redirection de ports sur chaque sous-réseau où se trouvent les récoltes pour que cette mise en place fonctionne correctement.


\newpage


Cette manière d'approcher le problème est utilisée plus judicieusement dans la troisième solution et permet d'éviter les problèmes mentionnés ci-dessus.
\vspace{12pt}\\
La seconde solution est probablement plus vite réalisée que la première, mais il se peut qu'elle offre moins de liberté aux niveaux des configurations. De surcroît, cette approche oblige l'utilisation de Home Assistant, ce qui n'est pas compatible avec le pipeline réalisé avec Docker proposé lors de ce projet. Cette solution n'est donc pas idéale dans le cadre de ce travail.
\vspace{12pt}\\
Les solutions 3 et 4 permettent d'éviter des problèmes de NAT et d'avoir un seul broker MQTT se trouvant dans le même sous-réseau que BBData. La différence est que la troisième nécessite des changements dans le code de BBData, tandis que la quatrième utilise l'API déjà fournie dans la version actuelle de la plateforme. Après discussion avec les personnes liées à ce projet, il a été décidé qu'il serait préférable de réaliser le Proof of Concept en suivant les principes de la quatrième approche, car elle offre une démarche un peu plus orientée "services" et il n'y a pas besoin de modifier le code de BBData.


\subsubsection{Activité 3 - Infrastructure de test BBData}
La réalisation de ce Proof of Concept nécessite un environnement de test de la plateforme BBData. Pour mettre en place cet environnement, le projet de semestre 5 de M. Mujynya \cite{ps5-bbdata-docker}, un ancien étudiant de la HEIA-FR, a été repris. Ce projet consistait à réaliser une version Docker et Kubernetes de la plateforme BBData, ce qui correspond parfaitement aux besoins de cette troisième activité.
\vspace{12pt}\\
La première étape consiste à "forker" le dépôt du PS5 de M. Mujynya, ce qui permet de récupérer les fichiers du dépôt et de les modifier selon les besoins de ce travail. Afin de tester l'environnement, les différents services de BBData ont été démarrés avec le fichier Docker Compose se trouvant dans le répertoire nommé "docker" du projet.
\vspace{12pt}\\
Une erreur est survenue lors du démarrage du service \textbf{cassandra}, qui est un système de gestion de base de données de type NoSQL. La résolution de ce problème se trouve dans le chapitre \textbf{"\nameref{obj3_problem}"} de cette activité.
\vspace{12pt}\\
Une fois ce problème résolu, l'application web d'administration de BBData est accessible à l'adresse "http://localhost:8088". Il faut pourtant trouver ou créer un utilisateur valide pour accéder à l'application. Pour cela, il a fallu se connecter à la base de données MySQL de BBData afin de trouver le nom de l'utilisateur administrateur créé par défaut. Les commandes utilisées se trouvent ci-dessous.

\begin{minted}{bash}
docker exec -it bbMySQL bash
mysql -h 127.0.0.1 -u bbdata-admin --password=bbdata bbdata2
\end{minted}

\begin{minted}{mysql}
show tables;
select * from users;
\end{minted}

\vspace{12pt}

L'administrateur créé par défaut est l'utilisateur \textbf{admin}. Le mot de passe de cet utilisateur est \textbf{testtest} et il a été trouvé dans le fichier "bbdata-api/docker/mysql/sql/test-data.sql", qui est le fichier SQL permettant d'ajouter les données par défaut dans les tables de la base de données. Avec ces informations, il est maintenant possible de se connecter sur l'application d'administration de BBData et de le paramétrer selon les besoins de cette activité.


\subsubsection{Activité 4 - Prototype de l'intégration}
\label{poc_activity4}
L'infrastructure de test est maintenant entièrement fonctionnelle et est prête à être adaptée pour réaliser un premier prototype.
\vspace{12pt}\\
Dans un premier temps, deux nouveaux services ont été ajoutés dans le fichier Docker Compose de la version Docker de BData. Ces 2 services sont le broker Mosquitto et le collecteur.
\vspace{12pt}\\
Pour le broker Mosquitto, la démarche a été identique à l'installation du broker sur le Raspberry Pi lors de la remise en place du pipeline. Le chapitre \textbf{"\nameref{appendix_broker_install}"} se trouvant en annexe donne tous les détails concernant cette mise en place. Finalement, dans le fichier "docker-compose.yml" de BBData en version Docker, le service suivant a été ajouté:

\begin{minted}{yaml}
### Mosquitto MQTT Broker ###
mosquitto:
  image: eclipse-mosquitto:2.0.15
  container_name: bbMosquitto
  restart: always
  networks:
    - bb-network
  volumes:
    - .containers/mosquitto/config/:/mosquitto/config/:ro
    - .containers/mosquitto/log/:/mosquitto/log/
    - .containers/mosquitto/data/:/mosquitto/data/
  ports:
    - 1883:1883
    - 9001:9001
\end{minted}


\newpage


Le script Sensbox a pu être repris pour la mise en place du collecteur, mais l'ajout d'une nouvelle fonctionnalité a tout de même été nécessaire afin que le script soit facilement adaptable à l'infrastructure de test de BBData.
\vspace{12pt}\\
La fonctionnalité ajoutée à Sensbox est une nouvelle option de démarrage du script permettant d'ajouter un fichier de configuration au format JSON qui spécifie l'instance de BBData à utiliser ainsi que les paramètres de connexion au broker MQTT. Ça permet ainsi d'éviter de devoir changer le code du script pour pouvoir modifier ces paramètres et ainsi produire une image Docker adaptable pour tous les cas d'utilisation possibles. Voici un exemple de ce nouveau fichier de configuration.

\begin{figure}[h]
\centering
\includegraphics[height=4cm]{images/resultats/obj3/config_sensbox.png}
\caption{Exemple du nouveau fichier de configuration JSON pour Sensbox}
\end{figure}

Dans la cas de ce prototype, un fichier Dockerfile se basant sur l'image de Sensbox a été créé. Ce dernier permet de copier les deux fichiers de configuration dans l'image, soit celui pour la liaison des topics avec les objets BBData ainsi que celui pour les paramètres de BBData et du broker. Un exemple du fichier de configuration pour la liaison topics et objets BBData se trouve dans le chapitre \textbf{"\nameref{obj2_improvement1}"}. Le script sera ensuite automatiquement exécuté en prenant en compte ces deux fichiers de configuration. Un aperçu de ce Dockerfile se trouve ci-dessous.

\begin{minted}{text}
FROM registry.forge.hefr.ch/noe.dupasqui/ps5-sensbox:latest

# Copy configs
COPY config.json .
COPY sensor_config.json .

CMD ["python3", ".", "start", "--config-sensor", "sensor_config.json",
	"--config", "config.json"]
\end{minted}


\newpage


Finalement, le collecteur peut être ajouté dans le fichier Docker Compose de BBData comme ci-dessous. Le répertoire "collector" contient le fichier Dockerfile ainsi que les deux fichiers de configuration nécessaires au collecteur.

\begin{minted}{yaml}
### Collector - bridge between Mosquitto broker and BBData API ###
collector:
  build: ./collector
  container_name: bbCollector
  restart: always
  networks:
    - bb-network
\end{minted}

\vspace{12pt}

Suite à l'ajout de ces deux services, la partie BBData est maintenant terminée. Il reste à effectuer quelques modifications au niveau du Raspberry Pi pour que le pipeline fonctionne sur l'environnement de test. Le Raspberry Pi va désormais exécuter uniquement deux services, soit le service \textbf{zwave-js-ui} permettant de récolter les données des capteurs Aeotec et le service \textbf{mqtt-netatmo-bridge} permettant de récolter les données des capteurs Netatmo.
\vspace{12pt}\\
Concernant les changements de configuration effectués, il y a d'abord l'\textbf{Host url} dans la configuration MQTT sur le serveur Z-Wave. Etant donné que les services liés à BBData sont démarrés sur un ordinateur portable, il faut entrer dans ce champ l'adresse IP de ce dernier. L'image ci-dessous montre ce changement. Dans le cas de ce travail, le nom de domaine local de l'ordinateur a été utilisé au lieu de son adresse IP, mais ceci donne le même résultat. Il faut dès lors modifier le nom d'utilisateur ainsi que le mot de passe du broker MQTT.

\begin{figure}[h]
\centering
\includegraphics[height=6cm]{images/resultats/obj3/change_zwave_mqtt_config.png}
\caption{Changement de configuration sur le serveur Z-Wave pour le Proof of Concept}
\end{figure}


\newpage


\subsubsection{Activité 5 - Evaluation du prototype}
\label{obj3_eval_prototype}
Pour terminer, le prototype est entièrement fonctionnel dans sa version en local, de la récolte des données des capteurs au stockage de ces dernières sur BBData, le tout en suivant l'approche d'intégration choisie lors de ce Proof of Concept. Cela prouve que cette mise en place est tout à fait réalisable sur la version en production de BBData. Il faut néanmoins faire attention que le broker MQTT soit accessible en dehors du sous-réseau de BBData, afin que les différents Raspberry Pi puissent envoyer les données des capteurs sur ce dernier.
\vspace{12pt}\\
Ce premier prototype fonctionne correctement. Cependant, il serait quand même important de tester le cas mentionné ci-dessus concernant le sous-réseau, afin de s'assurer que cela est bien réalisable sur la version en production.
\vspace{12pt}\\
Un autre point est que le prototype a été testé avec la version Docker de BBData, mais la version en production utilise peut être une autre approche, comme par exemple Kubernetes. Il faut donc s'assurer que les adaptations effectuées sur ce prototype soient également compatibles avec la version en production.
\vspace{12pt}\\
Le dernier point à vérifier est l'envoi de données de plusieurs stations Netatmo sur un même broker MQTT. Il se peut que la librairie utilisée ne soit compatible uniquement avec une seule station Netatmo. Il faudrait par conséquent adapter la librairie pour pouvoir spécifier le nom des topics à utiliser pour chaque récolte de données.
\vspace{12pt}\\
Pour conclure, ce premier prototype donne un bel aperçu de la nouvelle approche du pipeline, mais comme expliqué ci-dessus, il reste encore plusieurs éléments à tester et adapter avant de pouvoir la rendre disponible sur la plateforme en production de BBData.


\subsubsection{Problèmes rencontrés}
\label{obj3_problem}
Ce chapitre traite des problèmes rencontrés lors de la réalisation de ce troisième objectif de ce projet de semestre.


\newpage


\paragraph{Activité 3 - Problème avec service Cassandra}
~~\vspace{6pt}\\
Lors du lancement du conteneur \textbf{bbCassandra}, ce dernier s'arrêtait et affichait l'erreur suivante:

\begin{figure}[h]
\centering
\includegraphics[height=1.1cm]{images/resultats/obj3/error_cassandra.png}
\caption{Erreur dans les logs du service cassandra}
\end{figure}

Après quelques recherches, cette erreur survient car un fichier du service cassandra contient des retours à la ligne de type \textbf{"\textbackslash{}r"}, ce qui est incompatible avec les scripts bash.
\vspace{12pt}\\
La suppression des \textbf{"\textbackslash{}r"} dans ce fichier a permis de résoudre ce problème. Cela a été fait dans le Dockerfile utilisé pour la création de l'image de Cassandra. Le nouveau fichier Dockerfile se trouve ci-dessous.

\begin{minted}{text}
FROM cassandra:3.11

# fix UTF-8 accents in init scripts
ENV LANG C.UTF-8

# add scripts to initialize the db
COPY bootstrap_data/*.csv /docker-entrypoint-initdb.d/
COPY bootstrap_data/schema.cql /docker-entrypoint-initdb.d/01.cql
COPY bootstrap_data/import_data.cql /docker-entrypoint-initdb.d/02.cql

COPY entrypoint.sh /

# Remove trailing \r character
# Solve the error: bash: '\r': command not found
RUN sed -i 's/\r$//' entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
\end{minted}