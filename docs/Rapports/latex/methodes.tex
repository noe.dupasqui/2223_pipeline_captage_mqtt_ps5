\section{Méthodes}
Ce chapitre a pour but de décrire les étapes effectuées ainsi que les méthodes utilisées qui permettront d'obtenir les résultats de ce travail. Ce dernier concerne plus particulièrement le premier objectif, soit l'analyse du pipeline existant.

% -----------------------------------------------------------------------------------------------
\subsection{Mise en place du système actuel}
Ce chapitre concerne la première activité de l'analyse, soit la mise en place du pipeline. Le Raspberry Pi du pipeline actuel utilise le système d'exploitation de Home Assistant. Dans ce travail, l'utilisation d'une approche plus virtuelle a été prise grâce aux images et conteneurs Docker. Cette solution permet une meilleure flexibilité et portabilité, comme par exemple la mise en place du pipeline sur des systèmes ayant déjà d'autres programmes ou sur des systèmes autres que des Raspberry Pi. Le désavantage de ce choix est qu'il prend un peu plus de temps, car nous devons configurer tous les services nous même, ce qui n'est pas le cas si nous utilisons le système d'exploitation d'Home Assistant, car ce dernier permet d'intégrer facilement une grande quantité de services.
\vspace{12pt}\\
Mon système contiendra en tout 5 conteneurs qui sont décrits ci-dessous:

\vspace{6pt}

\begin{itemize}
\item \textbf{homeassistant}: pour visualiser les données des capteurs Aeotec et Netatmo. L'utilisation de ce service est devenu optionnel avec l'approche Docker, mais il est tout de même utile lors la mise en place du système
\item \textbf{zwave-js-ui}: permet de gérer les capteurs de la marque Aeotec. Ce service s'occupe également de transmettre les données de ces capteurs au broker MQTT
\item \textbf{mqtt-netatmo-js}: sert de passerelle pour la récupération des données des capteurs Netatmo et la transmission au broker MQTT
\item \textbf{mosquitto}: le broker MQTT qui réceptionnera les données des capteurs sur des topics spécifiques
\item \textbf{sensbox}: le script s'occupant d'envoyer les données sur la plateforme BBData. Sert de passerelle entre le broker MQTT et BBData.
\end{itemize}


\newpage


Ci-dessous se trouve un diagramme de composants permettant de mieux visualiser les différents services du pipeline ainsi que leurs communications.

\begin{figure}[h]
\centering
\includegraphics[height=12cm]{images/methodes/schema_services_compose_v4.png}
\caption{Diagramme de composants des services conteneurisés}
\end{figure}

\subsubsection{Installation du Raspberry Pi OS}
La toute première étape de cet objectif a été la mise en place du Rasperry Pi, car c'est l'élément central du pipeline. Il permet de faire la passerelle entre les capteurs et BBData et contient tous les services Docker, il est par conséquent essentiel au fonctionnement du système.
\vspace{12pt}\\
Il faut donc installer le système d'exploitation de Raspberry Pi sur une carte microSD. Lors de ce travail, la version LITE en 64 bits a été choisie car une interface graphique n'est pas nécessaire et le Raspberry Pi 4 Model B possède l'architecture ARMv8 en 64 bits. Pour cette réalisation, il existe un petit programme nommé Raspberry Pi Imager \cite{pi-imager} qui est très simple d'utilisation et qui se présente comme sur la figure suivante.

\begin{figure}[h]
\centering
\includegraphics[height=5cm]{images/methodes/rasperry_os.png}
\caption{Raspberry Pi Imager}
\end{figure}


\newpage


Lors de l'installation, les paramètres suivants ont été appliqués:

\vspace{6pt}

\begin{itemize}
\item Activation SSH
\item Définition du nom d'utilisateur et mot de passe
\item Configuation du WLAN
\item Configuration de la "time zone" et du clavier
\end{itemize}

\vspace{12pt}

Concernant le réseau WLAN, le Raspberry est connecté à un partage de connexion effectué depuis un smartphone. Un laptop est également connecté à ce réseau, ce qui permet d'accéder au Raspberry Pi en SSH et également d'avoir une connexion internet pour les différentes installations qui seront réalisées par la suite du projet.


\subsubsection{Installation d'Home Assistant}
Il existe plusieurs manières d'installer Home Assistant \cite{ha-installation} sur un Raspberry Pi. Une des méthode est de flasher directement le système d'exploitation de Home Assistant sur une carte microSD. Il suffira ensuite de l'insérer dans le Raspberry Pi et brancher le câble d'alimentation.
\vspace{12pt}\\
L'alternative choisie lors de ce travail est l'utilisation de l'image Docker de Home Assistant. L'installation est cependant un peu plus longue et demande un peu plus de configurations, mais elle apporte les avantages des environnements virtuels, comme par exemple la facilité d'intégration du pipeline sur différents systèmes. Par conséquent, la première étape a donc été d'installer Docker et Docker Compose sur le Raspberry Pi. Les commandes utilisées pour l'installation sont décrites dans l'annexe \textbf{"\nameref{appendix_docker_install}"}.
\vspace{12pt}\\
Les services seront décrits dans un fichier YAML qui sera utilisé par Docker Compose. Pour plus d'informations concernant la définition du service Home Assistant, vous pouvez vous référer à l'annexe \textbf{"\nameref{appendix_ha_install}"} se trouvant tout en bas de ce document.
\vspace{12pt}\\
Une fois le service démarré, Home Assistant est accessible avec un navigateur à l'adresse "http://raspberrypi:8123/", où "raspberrypi" peut aussi être remplacé par l'adresse IP du Raspberry Pi.


\subsubsection{Installation des capteurs Netatmo}
L'intégration des deux capteurs Netatmo (modules intérieur et extérieur) sur Home Assistant est plutôt simple. Il faut d'abord installer l'application IOS ou Android nommé "Netatmo Weather" sur un smartphone. Il faudra ensuite créer un compte et connecter les capteurs à un réseau Wi-Fi, se qui permettra aux capteurs d'envoyer leurs mesures sur un cloud hébergé par l'entreprise Netatmo. Une fois les capteurs connectés à internet, il faut ajouter l'intégration Netatmo dans Home Assistant comme représenté ci-dessous.

\begin{figure}[h]
\centering
\includegraphics[height=4cm]{images/methodes/netatmo_integration_ha.png}
\caption{Ajout de l'intégration Netatmo sur Home Assistant}
\end{figure}

L'intégration redirige sur une page de connexion de Netatmo. Il suffit de se connecter avec le compte créé préalablement et les capteurs seront automatiquement ajoutés sur Home Assistant.
\vspace{12pt}\\
Pour envoyer les données sur un broker MQTT, il faudra effectuer des configurations supplémentaires qui sont expliquées dans le chapitre \textbf{"\nameref{resultats}"}.


\subsubsection{Installation des capteurs Aeotec}
Les capteurs MultiSensor 6 de Aeotec nécessitent l'utilisation d'un dongle Z-Stick 5 ou 7 ainsi qu'un serveur Z-Wave. La première étape consiste à connecter le dongle USB sur le Raspberry Pi. Ensuite, il faut installer le serveur Z-Wave, qui sera dans la forme d'un service Docker Compose, comme tous les autres services. Pour cela, un nouveau service \cite{zwave-docker} est ajouté dans le fichier YAML. Les détails de cette installation sont décrites dans l'annexe \textbf{"\nameref{appendix_aeotec_install}"}. 
\vspace{12pt}\\
Une fois les capteurs connectés, nous pouvons visualiser leurs données grâce au tableau de bord de Home Assistant. Home Assistant offre également la possibilité de séparer les capteurs dans différentes zones, ce qui permet une meilleure vision d'ensemble lorsque le nombre de capteurs augmente sur le système. Le tableau de bord obtenu lors de ce travail correspond à la figure ci-dessous.

\begin{figure}[h]
\centering
\includegraphics[height=7cm]{images/methodes/ha_dashboard.png}
\caption{Tableau de bord de Home Assistant}
\end{figure}


\subsubsection{Installation du broker MQTT}
Comme pour les autres services, la déclaration du broker Mosquitto \cite{mosquitto-docker} a été ajoutée dans le fichier YAML. Ce broker MQTT va permettre de transférer les données de nos différents capteurs sur le logiciel Sensbox. Ce dernier va à son tour envoyer ces données sur BBData. Pour plus de détails concernant cette installation, veuillez vous référer à l'annexe \textbf{"\nameref{appendix_broker_install}"}.
\vspace{12pt}\\
Le détail des configurations nécessaires pour l'envoi des données des capteurs Aeotec sur le broker se trouve également en annexe: \textbf{"\nameref{appendix_aeotec_2_mqtt}"}.


\subsubsection{Installation de Sensbox}
La dernière étape du pipeline est l'envoi des données à BBData par l'intermédiaire du script Sensbox. Il a premièrement fallu cloner le dépôt Git sur le Raspberry Pi. Ce dernier a été créé par M. Montet et se trouve sur le serveur Gitlab de la HEIA-FR. Il est écrit en Python et utilise Poetry \cite{poetry} pour gérer les dépendances du programme.
\vspace{12pt}\\
La première idée était de réaliser une image Docker pour le programme Sensbox, mais plusieurs problèmes sont survenus avec l'utilisation de Poetry dans un conteneur Docker. Il a alors été décidé de renoncer temporairement à cette mise en place et de faire fonctionner le logiciel directement sur le Raspberry Pi sans passer par un conteneur et d'envisager cette mise en place lors de l'objectif 2 de ce travail concernant l'amélioration du système actuel. Les commandes utilisées pour l'installation se trouvent dans l'annexe \textbf{"\nameref{appendix_sensbox_install}"}.


% -----------------------------------------------------------------------------------------------
\subsection{Alternatives aux technologies actuelles}
Il existe probablement des technologies alternatives qui seraient utilisables pour la réalisation de ce pipeline. Ce chapitre a pour but de présenter et comparer quelques-unes de ces technologies dans le but d'explorer d'autres possibilités d'implémentation et de voir s'il y aurait potentiellement des changements technologiques à effectuer sur le pipeline actuel.


\subsubsection{Logiciels de domotiques}
\paragraph{OpenHAB}
~~\vspace{6pt}\\
Tout comme Home Assistant, OpenHAB \cite{openhab} est un logiciel libre destiné à la domotique. Ce dernier est développé en Java contrairement à Home Assistant qui est écrit en Python, et fourni également des add-ons pour le matériel utilisé dans ce travail, soit les 2 types de capteurs ainsi qu'une compatibilité avec le protocole MQTT. Ce logiciel pourrait tout à fait remplacer le logiciel Home Assistant, sauf qu'il n'y a pas réellement d'intérêt dans cette situation car il n'apporte rien de plus par rapport à Home Assistant. Selon un article comparant les deux logiciels \cite{openhab-vs-homeassistant}, les mises à jour de ce dernier sont plus complexes et plus coûteux en temps que sur Home Assistant.


\paragraph{Domoticz}
~~\vspace{6pt}\\
Domoticz \cite{domoticz} a l'avantage d'être très léger. Il peut être installé sur les Raspberry Pi, Windows, Linux, macOS et il existe même en image Docker. Il est possible de trouver des tutoriels expliquant comment intégrer les capteurs de ce projet, mais l'intégration de ces derniers est bien plus complexe que sur Home Assistant. Etant donné que le planning de ce travail est déjà bien chargé, il a été décidé qu'il serait plus judicieux de continuer avec Home Assistant et de se concentrer sur les points les plus importants de ce projet.


\subsubsection{Protocoles de communication}
Une fonctionnalité intéressante pour le pipeline actuel serait l'intégration d'autres protocoles de communication afin d'agrandir les possibilités de sources de données. Un protocole qui aurait un intérêt dans ce cas serait WebSocket car il nous permettrait de récupérer des données provenant du Web de manière fiable.


% -----------------------------------------------------------------------------------------------
\subsection{Problèmes du système actuel}
Après avoir mis en place le pipeline et effectué quelques tests, je n'ai pas remarqué de problèmes particuliers avec celui-ci. Effectivement, le pipeline fonctionne correctement, cependant des améliorations pourraient être faites, notamment au niveau du script Sensbox. Ces dernières sont décrites dans le chapitre ci-dessous.


% -----------------------------------------------------------------------------------------------
\subsection{Améliorations à apporter au système actuel}
Suite à une discussion avec M. Montet, voici les idées d'améliorations qui peuvent être apportées au système actuel:

\vspace{6pt}

\begin{itemize}
\item Amélioration des scripts de configuration et de démarrage de Sensbox (setup.sh, start.sh, stop.sh) afin qu'ils se lancent par défaut en arrière-plan avec un système de logging
\item Ajout et suppression automatique d'objets BBData ainsi que leurs assignations à des groupes d'objets lors du démarrage des scripts
\item Amélioration de la gestion des "Timestamps" dans le pipeline d'acquisition
\item Possibilité de réinstaller un système en une seule ligne de commande
\item Création d'un outil permettant de vérifier le statut de connexion de tout le système d'acquisition en une visualisation
\item Création d'un bot envoyant des alertes si une partie du système est défaillant
\item Récupération de données via le protocole WebSocket
\item Récupération de données provenant de MeteoSwiss
\end{itemize}

\vspace{12pt}

La sélection et la réalisation d'une partie de ces améliorations sera effectuée lors du deuxième objectif principal de ce projet, soit l'objectif concernant l'amélioration du système existant.


% -----------------------------------------------------------------------------------------------
\subsection{Home Assistant OS VS Docker}
\label{ha_vs_docker}
Comme mentionné précédemment dans le rapport, il y a deux approches principales pour utiliser Home Assistant, soit le système d'exploitation d'Home Assistant ou son image Docker. Ces deux approches ont leurs avantages et leurs inconvénients qui vont être expliqués dans ce chapitre.
\vspace{12pt}\\
En premier lieu, le système d'exploitation d'Home Assistant offre la possibilité d'ajouter des add-ons facilitant grandement l'intégration d'une multiple variété de capteurs, de services et d'applications. Cette fonctionnalité n'est pas disponible sur la version Docker de Home Assistant, ce qui signifie que c'est à nous d'installer et d'intégrer manuellement ces services sans passer par Home Assistant.
\vspace{12pt}\\
L'avantage de l'approche avec Docker est qu'il offre beaucoup plus de liberté en terme de configuration des services, et permet d'ajouter des services qui ne sont pas forcément disponibles dans les add-ons d'Home Assistant. Les inconvénients de cette approche est qu'elle demande davantage de mise en place due aux configurations plus nombreuses, ainsi que la gestion de ces services qui n'est plus centralisée sur Home Assistant et ces derniers doivent être gérés individuellement. Dans ce travail, étant donné que tous les services fonctionnent indépendamment d'Home Assistant, ce dernier n'est plus indispensable et sert uniquement d'outil de visualisation des données des capteurs. Grâce à l'utilisation de Docker Compose, cette approche offre la fonctionnalité de démarrer et d'arrêter tous les services nécessaires au pipeline en une seule ligne de commande, ce qui permet de gagner du temps lors de la mise en place d'une nouvelle pipeline.
\vspace{12pt}\\
Pour conclure, le plus grand avantage de Home Assistant OS est qu'il permet de centraliser la gestion et la configuration de nos services à un seul endroit. La mise en place est plus rapide grâce aux add-ons facilitant amplement l'intégration de nos différents services. Toutefois, il offre moins de liberté en terme de configurations car Home Assistant gère la plupart des éléments à notre place. Un grand inconvénient de cette approche est qu'elle rend le système dépendant de Home Assistant. Ce qui signifie que si le système nécessite un service ou une application qui n'est pas disponible sur Home Assistant, l'intégration de ce dernier serait compliqué voir impossible et par conséquent cela peut amener à devoir utiliser une autre approche, comme par exemple l'utilisation de l'image Docker de Home Assistant.


% -----------------------------------------------------------------------------------------------
\subsection{Conclusion de l'analyse}
Cette phase d'analyse a permis de mieux connaître et comprendre le pipeline actuel. Bien sûr, avant de mettre en place l'infrastructure, il y a avait plusieurs éléments qui restaient flous concernant son fonctionnement. Suite à cette analyse, les incertitudes sur le système ont été comblées et le pipeline a pu être mis en place selon une nouvelle approche, qui est l'utilisation de Docker. Le dernier bénéfice de cette analyse est la découverte des améliorations pouvant être effectuées sur le pipeline, ce qui permettra d'entamer le deuxième objectif de ce projet de semestre.


\newpage